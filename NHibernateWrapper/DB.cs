﻿using NHibernate;
using System;

namespace NHibernateWrapper
{
    public class DB
    {
        public DB(ISessionFactory _sessionFactory)
        {
          sessionFactory = _sessionFactory;
        }
        public ISessionFactory sessionFactory { set; get; }
        public ISession OpenSession()
        {
            return sessionFactory.OpenSession();
        }
        /// <summary>
        /// Automatic begin transaction and commit after action completed
        /// use for sensitive data that need to lock the table
        /// </summary>
        /// <param name="act"></param>
        /// <returns></returns>
        public bool SessionSafe(Action<ISession> act, ref Exception exception)
        {
            using (var session = sessionFactory.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {
                    try
                    {
                        act(session);
                        tx.Commit();
                        return true;
                    }
                    catch (Exception e)
                    {
                        Global.LogHandler.Error(e);
                        exception = e;
                        tx.Rollback();
                    }
                }
            }
            return false;
        }


        /// <summary>
        /// Does not lock the table, use for query
        /// </summary>
        /// <param name="act"></param>
        /// <param name="exception">Default will log the exception, incase user need to show in UI</param>
        /// <returns></returns>
        public bool Session(Action<ISession> act,ref Exception exception)
        {
            using(var session = sessionFactory.OpenSession())
            {
                try
                {
                    act(session);
                    return true;
                }
                catch(Exception ex)
                {
                    Global.LogHandler.Error(ex);     
                    exception = ex;
                }                
            }
            return false;
        }

    }
}
