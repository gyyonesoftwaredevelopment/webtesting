﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace NHibernateWrapper
{
    public class MySQLConnection: DBConnection
    {
        public MySQLConnection(string host, string dbname, int port, string userid, string password, bool updateSchema = false) : base(host, dbname, port, userid, password, updateSchema)
        {
        }

        public override string ConnectionString
        {
            get { return $"Server={Host};Port={Port};Uid={UserId};Pwd={Password};"; }
           
        }
        public override string DatabaseConnectionString
        {
            get
            {
                return $"Server={Host};Port={Port};Uid={UserId};Pwd={Password};Database={Database};";
            }

        }


        public override List<TableSchema> GetSchema()
        {
            var schema = new Dictionary<string, TableSchema>();

            using (var connection = new MySqlConnection(ConnectionString))
            {
                connection.Open();

                // Retrieve columns information
                var columnsQuery = @"
        SELECT 
            c.TABLE_NAME, 
            c.COLUMN_NAME, 
            c.DATA_TYPE, 
            c.IS_NULLABLE, 
            tc.CONSTRAINT_TYPE, 
            kcu.REFERENCED_TABLE_NAME AS RelatedTable, 
            kcu.REFERENCED_COLUMN_NAME AS RelatedColumn
        FROM 
            INFORMATION_SCHEMA.COLUMNS c
        LEFT JOIN 
            INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
        ON 
            c.TABLE_NAME = kcu.TABLE_NAME 
        AND 
            c.COLUMN_NAME = kcu.COLUMN_NAME
        LEFT JOIN 
            INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
        ON 
            kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME
        AND 
            tc.CONSTRAINT_TYPE IN ('PRIMARY KEY', 'FOREIGN KEY')
        WHERE 
            c.TABLE_SCHEMA = @DatabaseName
        ORDER BY 
            c.TABLE_NAME, c.ORDINAL_POSITION";

                using (var command = new MySqlCommand(columnsQuery, connection))
                {
                    command.Parameters.AddWithValue("@DatabaseName", connection.Database);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string tableName = reader["TABLE_NAME"].ToString();

                            if (!schema.ContainsKey(tableName))
                            {
                                schema[tableName] = new TableSchema
                                {
                                    TableName = tableName
                                };
                            }

                            var columnInfo = new ColumnSchema
                            {
                                ColumnName = reader["COLUMN_NAME"].ToString(),
                                DataType = reader["DATA_TYPE"].ToString(),
                                IsNullable = reader["IS_NULLABLE"].ToString() == "YES",
                                ConstraintType = reader["CONSTRAINT_TYPE"]?.ToString(),
                                RelatedTable = reader["RelatedTable"]?.ToString(),
                                RelatedColumn = reader["RelatedColumn"]?.ToString()
                            };

                            schema[tableName].Columns.Add(columnInfo);
                        }
                    }
                }
            }

            return schema.Values.ToList();
        }
        public override List<TableSchemaFK> GetTableSchemsFK()
        {
            var schemaGroups = new Dictionary<(string SchemaName, string TableName,string ReferencedTable, string ForeignKeyName), TableSchemaFK>();

            string query = @"
        SELECT 
            TABLE_SCHEMA AS SchemaName,
            TABLE_NAME AS TableName,
            COLUMN_NAME AS ColumnName,
            CONSTRAINT_NAME AS ForeignKeyName,
            REFERENCED_TABLE_NAME AS ReferencedTable,
            REFERENCED_COLUMN_NAME AS ReferencedColumn
        FROM 
            INFORMATION_SCHEMA.KEY_COLUMN_USAGE
        WHERE 
            REFERENCED_TABLE_NAME IS NOT NULL
        ORDER BY 
            TABLE_SCHEMA, TABLE_NAME, CONSTRAINT_NAME, COLUMN_NAME";

            using (var connection = new MySqlConnection(DatabaseConnectionString))
            {
                var command = new MySqlCommand(query, connection);
                connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var key = (
                            SchemaName: reader["SchemaName"].ToString(),
                            TableName: reader["TableName"].ToString(),
                            ReferencedTable: reader["ReferencedTable"].ToString(),
                            ForeignKeyName: reader["ForeignKeyName"].ToString()
                        );

                        if (!schemaGroups.ContainsKey(key))
                        {
                            schemaGroups[key] = new TableSchemaFK
                            {
                                SchemaName = key.SchemaName,
                                TableName = key.TableName,
                                ReferencedTable = key.ReferencedTable,
                                ForeignKeyName = key.ForeignKeyName,                              
                                ColumnMappings = new List<ColumnMappingInfo>()
                            };
                        }

                        schemaGroups[key].ColumnMappings.Add(new ColumnMappingInfo
                        {
                            ColumnName = reader["ColumnName"].ToString(),                     
                            ReferencedColumn = reader["ReferencedColumn"].ToString()
                        });
                    }
                }
            }

            return schemaGroups.Values.ToList();
        }
    }
}
