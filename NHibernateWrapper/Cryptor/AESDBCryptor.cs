﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace NHibernateWrapper.Cryptor
{

    public static class AESDBCryptor
    {
        public static byte[] DefaultKey = new byte[32] {
                0x13, 0x25, 0xF3, 0xF8, 0xA1, 0xD3, 0x07, 0x08,
                0xD1, 0xC0, 0xB3, 0xF2, 0xC2, 0x4A, 0x4B, 0x6D,
                0x8D, 0xC1, 0xA3, 0x3D, 0x9A, 0xFF, 0x44, 0x78,
                0x51, 0x70, 0x81, 0xBA, 0x12, 0x5C, 0x9B, 0x4D
            };
        public static byte[] DefaultIV = {
            0x78, 0x3D, 0x55, 0xF8, 0x49, 0xD5, 0xC1, 0x68,
            0xFA, 0xCD, 0xB5, 0xFB, 0xBC, 0x04, 0x15, 0x36 };
        private static byte[] ToByteFormat(this string text)
        {
            return Encoding.Default.GetBytes(text);
        }
        private static string ToStringFormat(this byte[] data)
        {
            return Encoding.Default.GetString(data);
        }
        public static string EncryptString(string text, byte[] key, byte[] iv)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return "";
            }
            byte[] encrypted = Encrypt(text.ToByteFormat(), key, iv);
            return Convert.ToBase64String(encrypted);
        }
        public static string EncryptString(string text, byte[] key, out byte[] iv)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                iv = new byte[0];

                return "";
            }
            byte[] encrypted = Encrypt(text.ToByteFormat(), key, out iv);
            return Convert.ToBase64String(encrypted);
        }
        public static string EncryptString(string text, out byte[] key, out byte[] iv)
        {
            byte[] encrypted = Encrypt(text.ToByteFormat(), out key, out iv);
            return Convert.ToBase64String(encrypted);
        }
        public static byte[] Encrypt(byte[] data, byte[] key, out byte[] iv)
        {
            using (Aes aes = Aes.Create())
            {
                // Create encryptor          
                iv = aes.IV;
                aes.Key = key;
                ICryptoTransform encryptor = aes.CreateEncryptor();
                // Create MemoryStream    
                using (MemoryStream ms = new MemoryStream())
                {
                    // Create crypto stream using the CryptoStream class. This class is the key to encryption    
                    // and encrypts and decrypts data from any given stream. In this case, we will pass a memory stream    
                    // to encrypt    
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        // Create StreamWriter and write data to a stream    
                        using (BinaryWriter bw = new BinaryWriter(cs))
                        {
                            bw.Write(data);

                        }
                        return ms.ToArray();
                    }
                }
            }
        }

        public static byte[] Encrypt(byte[] data, out byte[] key, out byte[] iv)
        {
            using (Aes aes = Aes.Create())
            {
                // Create encryptor          
                iv = aes.IV;
                key = aes.Key;
                ICryptoTransform encryptor = aes.CreateEncryptor();
                // Create MemoryStream    
                using (MemoryStream ms = new MemoryStream())
                {
                    // Create crypto stream using the CryptoStream class. This class is the key to encryption    
                    // and encrypts and decrypts data from any given stream. In this case, we will pass a memory stream    
                    // to encrypt    
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        // Create StreamWriter and write data to a stream    
                        using (BinaryWriter bw = new BinaryWriter(cs))
                        {
                            bw.Write(data);

                        }
                        return ms.ToArray();
                    }
                }
            }
        }

        public static byte[] Encrypt(byte[] data, byte[] key, byte[] iv)
        {
            using (Aes aes = Aes.Create())
            {
                // Create encryptor          
                aes.IV = iv;
                aes.Key = key;
                ICryptoTransform encryptor = aes.CreateEncryptor();
                // Create MemoryStream    
                using (MemoryStream ms = new MemoryStream())
                {
                    // Create crypto stream using the CryptoStream class. This class is the key to encryption    
                    // and encrypts and decrypts data from any given stream. In this case, we will pass a memory stream    
                    // to encrypt    
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        // Create StreamWriter and write data to a stream    
                        using (BinaryWriter bw = new BinaryWriter(cs))
                        {
                            bw.Write(data);

                        }
                        return ms.ToArray();
                    }
                }
            }
        }
        public static string DecryptString(string text, byte[] key, byte[] iv)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return "";
            }

            byte[] decrypted = Decrypt(Convert.FromBase64String(text), key, iv);

            return decrypted.ToStringFormat();
        }
        public static byte[] Decrypt(byte[] encryptdata, byte[] key, byte[] iv)
        {
            List<byte> data = new List<byte>();
            using (Aes aes = Aes.Create())
            {
                // Create encryptor          
                aes.Key = key;
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor();

                // Create MemoryStream    
                using (MemoryStream ms = new MemoryStream(encryptdata))
                {
                    // Create crypto stream using the CryptoStream class. This class is the key to encryption    
                    // and encrypts and decrypts data from any given stream. In this case, we will pass a memory stream    
                    // to encrypt    
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Read))
                    {
                        // Create StreamWriter and write data to a stream    
                        using (BinaryReader reader = new BinaryReader(cs))
                        {
                            byte[] chunk;

                            chunk = reader.ReadBytes(1024);
                            while (chunk.Length > 0)
                            {
                                data.AddRange(chunk);
                                chunk = reader.ReadBytes(1024);
                            }
                        }
                    }
                }
            }
            return data.ToArray();

        }

        public static string DefaultEncryptString(string text)
        {
            //In Certain Condition, we need default IV and Key
            //Where we Do not have place to store the key and IV
            return AESDBCryptor.EncryptString(text, DefaultKey, DefaultIV);
        }
        public static string DefaultDecryptString(string text)
        {
            //In Certain Condition, we need default IV and Key
            //Where we Do not have place to store the key and IV
            return AESDBCryptor.DecryptString(text, DefaultKey, DefaultIV);
        }
        public static byte[] DefaultEncrypt(byte[] data)
        {
            //In Certain Condition, we need default IV and Key
            //Where we Do not have place to store the key and IV
            return AESDBCryptor.Encrypt(data, DefaultKey, DefaultIV);
        }
        public static byte[] DefaultDecrypt(byte[] data)
        {
            //In Certain Condition, we need default IV and Key
            //Where we Do not have place to store the key and IV
            return AESDBCryptor.Decrypt(data, DefaultKey, DefaultIV);
        }


        public static void GenerateKeyAndIV(out byte[] key, out byte[] iv)
        {
            using (Aes aes = Aes.Create())
            {
                // Create encryptor          
                iv = aes.IV;
                key = aes.Key;
            }
        }
        public static byte[] GeneratIV()
        {
            using (Aes aes = Aes.Create())
            {
                // Create encryptor          
                return aes.IV;
            }
        }
        public static byte[] GeneratKey()
        {
            using (Aes aes = Aes.Create())
            {
                // Create encryptor          
                return aes.Key;
            }
        }
    }
}
