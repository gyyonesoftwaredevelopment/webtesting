﻿using Newtonsoft.Json;

namespace NHibernateWrapper.ColumnType
{
    public class EncryptFormat
    {
        public EncryptFormat()
        {

        }
        public EncryptFormat(byte[] data, byte[] keys, byte[] iv, string dataType)
        {
            Data = data;
            Key = keys;
            IV = iv;
            DataType = dataType;
        }
        public EncryptFormat(string text, byte[] key, byte[] iv, string dataType)
        {
            Data = System.Text.Encoding.UTF8.GetBytes(text);
            Key = key;
            IV = iv;
            DataType = dataType;
        }
        public EncryptFormat(string text, string salt, string dataType)
        {
            Data = System.Text.Encoding.UTF8.GetBytes(text);
            Salt = salt;
            DataType = dataType;
        }
        public string DataStr
        {
            get
            {
                return System.Text.Encoding.UTF8.GetString(Data);
            }
        }
        [JsonProperty("D")]
        public byte[] Data { set; get; }
        [JsonProperty("I")]
        public byte[] IV { set; get; }
        [JsonProperty("K")]
        public byte[] Key { set; get; }
        [JsonProperty("S")]        
        public string Salt { set; get; }
        [JsonProperty("T")]
        public string DataType { set; get; }

    }
}
