﻿using Newtonsoft.Json;
using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using System.Text;
using NHibernateWrapper.Cryptor;

namespace NHibernateWrapper.ColumnType
{
    /// <summary>
    /// EncryptColumnType min Lenght = 66 + Your Enrypted Text Lenght
    /// </summary>
    [Serializable]
    public class AESEncryptColumnType : IUserType
    {
        public new bool Equals(object x, object y)
        {
            if (object.ReferenceEquals(x, y))
                return true;

            if (x == null || y == null)
                return false;

            return x.Equals(y);
        }

        public int GetHashCode(object x)
        {
            if (x == null)
                return 0;

            return x.GetHashCode();
        }



        public object DeepCopy(object value)
        {
            return value;
        }

        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        public object Assemble(object cached, object owner)
        {
            return cached;
        }

        public object Disassemble(object value)
        {
            return value;
        }

        public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
        {
            // rs.getst

            var obj = rs[names[0]];
            try
            {
                if (string.IsNullOrWhiteSpace(obj.ToString()))
                {
                    return null;
                }
                string json = AESDBCryptor.DefaultDecryptString(obj.ToString());
                if (string.IsNullOrEmpty(json))
                {
                    return null;
                }
                EncryptFormat enFormat = JsonConvert.DeserializeObject<EncryptFormat>(json);
                if (enFormat == null)
                {
                    return enFormat;
                }
                string decryptedData = AESDBCryptor.DecryptString(enFormat.DataStr, enFormat.Key, enFormat.IV);
                return JsonConvert.DeserializeObject(decryptedData, Type.GetType(enFormat.DataType));
            }
            catch (Exception ex)
            {
                //Possible Cannot Decrypt            
                Console.Error.WriteLine(ex.ToString());
            }
            return null;

        }

        public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
        {
            var parameter = (DbParameter)cmd.Parameters[index];
            if (value == null)
            {
                parameter.Value = null;
            }
            else
            {
                byte[] key;
                byte[] iv;
                string encrypted = AESDBCryptor.EncryptString(JsonConvert.SerializeObject(value), out key, out iv);
                var encrypFormat = new EncryptFormat(encrypted, key, iv, value.GetType().FullName);
                parameter.Value = AESDBCryptor.DefaultEncryptString(JsonConvert.SerializeObject(encrypFormat));
            }
        }

        public SqlType[] SqlTypes
        {
            get
            {
                return new SqlType[] { new SqlType(DbType.String) };
            }
        }

        public Type ReturnedType
        {
            get { return typeof(object); }
        }

        public bool IsMutable
        {
            get { return false; }
        }
    }
}
