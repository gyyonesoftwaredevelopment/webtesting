﻿using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;
using System;
using System.Data.Common;
using System.Data;
using Newtonsoft.Json;
using NHibernateWrapper.Cryptor;

namespace NHibernateWrapper.ColumnType
{
    /// <summary>
    /// EncryptColumnType min Lenght = 66 + Your Enrypted Text Lenght
    /// </summary>
    [Serializable]
    public class OneWayHashColumnType : IUserType
    {

        public new bool Equals(object x, object y)
        {
            if (object.ReferenceEquals(x, y))
                return true;

            if (x == null || y == null)
                return false;

            return x.Equals(y);
        }

        public int GetHashCode(object x)
        {
            if (x == null)
                return 0;

            return x.GetHashCode();
        }



        public object DeepCopy(object value)
        {
            return value;
        }

        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        public object Assemble(object cached, object owner)
        {
            return cached;
        }

        public object Disassemble(object value)
        {
            return value;
        }

        public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
        {
            // rs.getst
            var obj = rs[names[0]];
            try
            {
                if (string.IsNullOrWhiteSpace(obj.ToString()))
                {
                    return null;
                }
                return obj.ToString();
 
            }
            catch (Exception ex)
            {
                //Possible Cannot Decrypt            
                Console.Error.WriteLine(ex.ToString());
            }
            return null;


            // return JsonConvert.DeserializeObject((string)owner, typeof(T));
        }

        public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
        {
            var parameter = (DbParameter)cmd.Parameters[index];
            if (value.GetType() == typeof(string))
            {
                string text = value.ToString();
                if (!string.IsNullOrEmpty(text))
                {            
                    parameter.Value = BCrypt.Net.BCrypt.HashPassword(text, BCrypt.Net.BCrypt.GenerateSalt());
                }
                else
                {
                    parameter.Value = "";
                }
            }
            else
            {
                throw new Exception($"Not Support Data Type : {value.GetType().FullName} . Expected :{typeof(string).FullName}");
            }


        }

        public SqlType[] SqlTypes
        {
            get
            {
                return new SqlType[] { new SqlType(DbType.String) };
            }
        }

        public Type ReturnedType
        {
            get { return typeof(string); }
        }

        public bool IsMutable
        {
            get { return false; }
        }
    }
}
