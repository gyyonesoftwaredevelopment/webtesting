﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace NHibernateWrapper
{
    public abstract class DBConnection
    {
        public virtual string Host { set; get; }
        public virtual int Port { set; get; }
        public virtual string Database { set; get; }
        public virtual string UserId { set; get; }
        [JsonConverter(typeof(PasswordConverter))]
        public virtual string Password { set; get; }
        //True: For connection own Databae, It will handle Create / Update table. Create New Field
        //False, Not doing anythings
        public virtual bool UpdateSchema { set; get; }
        public DBConnection(string host, string dbname, int port, string userid, string password,bool updateSchema = false) {
            Host = host;
            Port = port;
            Database = dbname;
            UserId = userid;
            Password = password;
            UpdateSchema = updateSchema;
        }
        public virtual List<TableSchema> GetSchema()
        {
            return new List<TableSchema>();
        }
        public virtual List<TableSchemaFK> GetTableSchemsFK()
        {
            return new List<TableSchemaFK>();
        }
       
        public virtual string ConnectionString
        {
            get { return ""; }
        }
        public virtual string DatabaseConnectionString
        {
            get { return ""; }
        }
    }
    public class TableSchemaFK
    {
        public string SchemaName { get; set; }
        public string TableName { get; set; }
        public string ForeignKeyName { get; set; }
        public string ReferencedTable { get; set; }
        public List<ColumnMappingInfo> ColumnMappings { get; set; }
    }

    public class ColumnMappingInfo
    {
        public string ColumnName { get; set; }   
        public string ReferencedColumn { get; set; }
    }

    public class TableSchema
    {
        public string TableName { get; set; }
        public List<ColumnSchema> Columns { get; set; } = new List<ColumnSchema>();
    }

    public class ColumnSchema
    {
        public string ColumnName { get; set; }
        public string DataType { get; set; }
        public bool IsNullable { get; set; }
        public string ConstraintType { get; set; }
        public string RelatedTable { get; set; }
        public string RelatedColumn { get; set; }
    }
}
