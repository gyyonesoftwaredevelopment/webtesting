﻿using FluentNHibernate.Mapping;
using System;

namespace NHibernateWrapper.Entity
{
    public class EntityAuditMap : SubclassMap<EntityAudit>
    {
        public EntityAuditMap()
        {
            //Id of User , but do not want to add any reference 
            References(x => x.CreatedBy).ForeignKey(null).Nullable().Cascade.None();
            References(x => x.UpdatedBy).ForeignKey(null).Nullable().Cascade.None();
            Map(x => x.CreatedOn);
            Map(x => x.UpdatedOn);
            Abstract();
        }
    }
    public class EntityAudit:EntityIdentity
    {
        public EntityAudit()
        {
            CreatedOn = DateTime.UtcNow;
            UpdatedOn = DateTime.UtcNow;
        }
        public virtual DateTime CreatedOn { set; get; }
        public virtual User CreatedBy { set; get; }
        public virtual DateTime UpdatedOn { set; get; }
        public virtual User UpdatedBy { set; get; }
    }
}
