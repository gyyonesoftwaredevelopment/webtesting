﻿using FluentNHibernate.Mapping;
using NHibernate.Mapping;
using NHibernateWrapper.Entity;
using System.Collections.Generic;

namespace NHibernateWrapper.Entity
{
    public class GroupMap : SubclassMap<Groups>
    {
        public GroupMap()
        {         
            Map(x => x.GroupId);       
            Map(x=> x.GroupDesc);

            Abstract();
            
          

        }
    }
    public class Groups : EntityIdentity
    {
        public Groups()
        {
          
        }
        public virtual string GroupId
        {
            get { return base.Id; }
            set { base.Id = value; }
        }   
        public virtual string GroupDesc { set; get; }
    

    }
    public class UserGroupMap : SubclassMap<UserGroup>
    {
        public UserGroupMap()
        {
            HasManyToMany(x => x.Users).Cascade.All(); // Handle cascading options (you can modify as per your needs)
        }
    }
    public class UserGroup : Groups
    {
        public UserGroup()
        {
            Users = new HashSet<User>();
        }
        public virtual string GroupId { set { base.Id = value; } get { return base.Id; } }

        public virtual ISet<User> Users { get; set; }
    }
    
}
