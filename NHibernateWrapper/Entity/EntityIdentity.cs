﻿using FluentNHibernate.Mapping;
using System;

namespace NHibernateWrapper.Entity
{
    public class EntityIdentityMap : ClassMap<EntityIdentity>
    {
        public EntityIdentityMap()
        {       
            Id(x => x.Id).Length(50);

            UseUnionSubclassForInheritanceMapping();
        }
    }
    public class EntityIdentity
    {
        public EntityIdentity()
        {
            Id = Guid.NewGuid().ToString("N");
     
        }
        public virtual string Id { set; get; }

    }

}
