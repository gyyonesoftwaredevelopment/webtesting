﻿using FluentNHibernate.Mapping;
using System;

namespace NHibernateWrapper.Entity
{
    public class ScriptHistoryMap : SubclassMap<ScriptHistory>
    {
        public ScriptHistoryMap()
        {
            Map(x => x.Version).Length(25);       
        }
    }
    public class ScriptHistory : EntityOrder
    {
        public virtual string Version { set; get; }
       
     
    }
}
