﻿
using FluentNHibernate.Mapping;
using System;

namespace NHibernateWrapper.Entity
{
    public class AppHistoryMap : SubclassMap<SchemaHistory>
    {
        public AppHistoryMap()
        {
            Map(x => x.Version).Length(25);         
            Map(x=> x.CreatedOn).Length(25);
        }
    }
    public class SchemaHistory : EntityIdentity
    {
        public SchemaHistory()
        {
            CreatedOn = DateTime.UtcNow;
        }
        public virtual string Version { set; get; }
        public virtual DateTime CreatedOn{set;get;}
      

    }
}
