﻿using FluentNHibernate.Mapping;
using System;

namespace NHibernateWrapper.Entity
{
    public class EntityOrderMap : SubclassMap<EntityOrder>
    {
        public EntityOrderMap()
        {      
            Map(x => x.SeqNo).Length(9);
        }
    }

    public class EntityOrder:EntityIdentity
    {
        public EntityOrder()
        {
            SeqNo = 0;
        }

        public virtual int SeqNo { set { base.Id = value.ToString(); } get{ return int.Parse(base.Id); } }
    }
    public class EntityAuditOrderMap : SubclassMap<EntityAuditOrder>
    {
        public EntityAuditOrderMap()
        {
            Map(x => x.SeqNo).Length(9);
        }
    }
    public class EntityAuditOrder:EntityAudit
    {
        public EntityAuditOrder()
        {
            Id = Guid.NewGuid().ToString("N");
        }
        public virtual int SeqNo { set; get; }

    }
}
