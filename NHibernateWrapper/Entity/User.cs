﻿using FluentNHibernate.Mapping;
using NHibernate.Mapping;
using NHibernateWrapper.ColumnType;
using NHibernateWrapper.Entity;
using System.Collections.Generic;

namespace NHibernateWrapper.Entity
{
    public class UserMap : SubclassMap<User>
    {
        public UserMap()
        {
            Map(x => x.UserId);
            Map(x => x.UserName).Nullable();
            Map(x=> x.Password).Length(2048).CustomType<AESEncryptColumnType>();
            HasManyToMany(x => x.UserGroups).Cascade.All().Inverse(); // Handle cascading options (you can modify as per your needs)

        }
    }
    public class User : EntityAudit
    {
        public User()
        {
            UserGroups = new HashSet<UserGroup>();
        }
       
        //Id to login

        public virtual string UserId { set { base.Id = value; } get { return base.Id; } }
      
        //Display Name
        public virtual string UserName { set; get; }
        public virtual string Password { set; get; }
        public virtual ISet<UserGroup> UserGroups { set; get; }
    }


}
