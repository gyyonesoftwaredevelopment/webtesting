﻿using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Cfg;
using NHibernate;
using System;
using System.Data;
using System.Reflection;
using MySqlConnector;
using NHibernate.Tool.hbm2ddl;
using System.Data.SqlClient;
using System.IO;
using log4net.Config;
using NHibernate.Mapping;
using System.Text;
using BCrypt.Net;
using NHibernateWrapper.Entity;

namespace NHibernateWrapper
{

    public static class DBFactory
    {
        private static ISessionFactory CreateSQLLiteSessionFactory(params Assembly[] assemblies)
        {
            return Fluently.Configure()
                .Database(SQLiteConfiguration.Standard.UsingFile("Lite.db")) // SQLite file path
                .Mappings(m => {
                    foreach(var a in assemblies)
                    {
                        m.FluentMappings.AddFromAssembly(a);
                    }
                
                }) // Mapping from assembly
                .BuildSessionFactory();
        }
        public static ISessionFactory CreateMySQLSessionFactory(MySQLConnection connection, string AppVersion, params Assembly[] assemblies)
        {
            Global.LogHandler.Debug($"Start {connection.ToString()} {AppVersion}");
            bool updateSchema = false;

            if (AppVersion.Equals("1.0.0.0"))
            {
                AppVersion = DateTime.Now.ToString("Dev yyyy-MM-dd HH:mm:ss");
            }

            if (connection.UpdateSchema)
            {
                using (MySqlConnection conn = new MySqlConnection(connection.ConnectionString))
                {
                    conn.Open();
                    using (MySqlCommand command = conn.CreateCommand())
                    {
                        command.CommandText = @"CREATE DATABASE IF NOT EXISTS `" + connection.Database + "`;";
                        command.ExecuteNonQuery();
                        updateSchema = true;
                    }
                }

                try
                {
                    using (MySqlConnection conn = new MySqlConnection(connection.DatabaseConnectionString))
                    {
                        conn.Open();
                        using (MySqlCommand command = new MySqlCommand($"SELECT CreatedOn, Version FROM {typeof(SchemaHistory).Name} WHERE Version = '{AppVersion}'", conn))
                        {
                            // Create a DataTable to store the result
                            DataTable dataTable = new DataTable();

                            // Load data from MySqlCommand into DataTable
                            using (MySqlDataReader reader = command.ExecuteReader())
                            {
                                dataTable.Load(reader);
                            }

                            if (dataTable.Rows.Count == 0)
                            {
                                updateSchema = true;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    updateSchema = true;
                }
            }

            var persistenceConfig = MySQLConfiguration.Standard
                              .ConnectionString(connection.DatabaseConnectionString).Driver<NHibernate.Driver.MySqlConnector.MySqlConnectorDriver>().Dialect<NHibernate.Dialect.MySQL57Dialect>();
            StringBuilder sqlscript = new StringBuilder();
            var factory = Fluently.Configure()
                  .Database(persistenceConfig)
                  .Mappings(m => {
                      foreach (var assembly in assemblies)
                      {
                          m.FluentMappings.AddFromAssembly(assembly);
                      }
                  })
                  .ExposeConfiguration((config) => {
                      config.SetProperty("adonet.batch_size", "100");
                    
                      //For mysql Schema update is conflict if there is same TableName and different Database. 
                      //Manual Script Update is required
                      
                      new SchemaUpdate(config).Execute(true, true);

                  })
                  .BuildSessionFactory();
            if(updateSchema && sqlscript.Length > 0)
            {
                using (var con = new MySqlConnection(connection.DatabaseConnectionString))
                {
                    try
                    {
                        // Open the connection
                        con.Open();

                        // Begin a transaction
                        using (var transaction = con.BeginTransaction())
                        {
                            try
                            {
                                // Create a command object and set the transaction
                                using (var command = con.CreateCommand())
                                {
                                    command.Transaction = transaction;
                                    command.CommandText = sqlscript.ToString();

                                    // Execute the script
                                    command.ExecuteNonQuery();
                                }
                                // Commit the transaction if successful
                                transaction.Commit();
                              
                                Console.WriteLine("Transaction committed successfully.");
                            }
                            catch (Exception ex)
                            {
                                // Rollback the transaction in case of an error
                                transaction.Rollback();
                                Console.Error.WriteLine($"Transaction rolled back. Error: {ex.Message}");
                                throw ex;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine($"Error: {ex.Message}");
                        throw ex;
                    }
                }
            }

            string sqlQuery = $"INSERT INTO {typeof(SchemaHistory).Name}  (Id, CreatedOn, Version) VALUES (@Id, @CreatedOn, @Version);";
            if (updateSchema)
            {
                using (MySqlConnection conn = new MySqlConnection(connection.DatabaseConnectionString))
                {
                    conn.Open();

                    // Create MySqlCommand
                    MySqlCommand cmd = new MySqlCommand(sqlQuery, conn);
                    // Add parameters
                    cmd.Parameters.AddWithValue("@CreatedOn", DateTime.UtcNow);
                    cmd.Parameters.AddWithValue("@Version", AppVersion);
                    cmd.Parameters.AddWithValue("@Id", Guid.NewGuid().ToString("N"));
                    // Execute command
                    int rowsAffected = cmd.ExecuteNonQuery();
                    if (rowsAffected == 0)
                    {
                        throw new Exception("Unable to Update AppHistory");
                    }
                }
            }
            return factory;
        }
        public static ISessionFactory CreateMSSQLSessionFactory(MSSQLConnection connection,string AppVersion, params Assembly[] assemblys)
        {
            Global.LogHandler.Debug($"Start {connection.ToString()} {AppVersion}");
            bool updateSchema = false;
            if(AppVersion.Equals("1.0.0.0"))
            {
                AppVersion = DateTime.Now.ToString("Dev yyyy-MM-dd HH:mm:ss");
            }
            if(connection.UpdateSchema)
            {
                using (SqlConnection conn = new SqlConnection(connection.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand command = conn.CreateCommand())
                    {
                        command.CommandText = @"IF NOT EXISTS (
    SELECT name 
    FROM master.sys.databases 
    WHERE name = '" + connection.Database + @"'
)
BEGIN
    CREATE DATABASE [" + connection.Database + @"];
END";
                        command.ExecuteNonQuery();
                        updateSchema = true;
                    }
                }

                try
                {
                    using (SqlConnection conn = new SqlConnection(connection.DatabaseConnectionString))
                    {
                        conn.Open();
                        using (SqlCommand command = new SqlCommand($"select CreatedOn, Version FROM {typeof(SchemaHistory).Name} Where Version = '{AppVersion}'", conn))
                        {
                            // Create a DataTable to store the result
                            DataTable dataTable = new DataTable();

                            // Load data from MySqlCommand into DataTable
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                dataTable.Load(reader);
                            }

                            if (dataTable.Rows.Count == 0)
                            {
                                updateSchema = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    updateSchema = true;
                }

            }
            

            var persistenceConfig = MsSqlConfiguration.MsSql2012.ConnectionString(connection.DatabaseConnectionString).ShowSql();
            var factory = Fluently.Configure()
                  .Database(persistenceConfig)
                  .Mappings(m => {
                      foreach (var assembly in assemblys)
                      {
                          m.FluentMappings.AddFromAssembly(assembly);
                      }
                  })
                  .ExposeConfiguration((config) => {
                      config.SetProperty("adonet.batch_size", "100");           

                      new SchemaUpdate(config).Execute(false, updateSchema);

                  })
                  .BuildSessionFactory();

   


            string sqlQuery = $"INSERT INTO {typeof(SchemaHistory).Name} (Id, CreatedOn, Version) VALUES (@Id, @CreatedOn, @Version);";
            if (updateSchema)
            {
                using (SqlConnection conn = new SqlConnection(connection.DatabaseConnectionString))
                {
                    conn.Open();

                    // Create MySqlCommand
                    SqlCommand cmd = new SqlCommand(sqlQuery, conn);
                    // Add parameters
                    cmd.Parameters.AddWithValue("@CreatedOn", DateTime.UtcNow);
                    cmd.Parameters.AddWithValue("@Version", AppVersion);
                    cmd.Parameters.AddWithValue("@Id", Guid.NewGuid().ToString("N"));
                    // Execute command
                    int rowsAffected = cmd.ExecuteNonQuery();
                    if (rowsAffected == 0)
                    {
                        throw new Exception($"Unable to Update {typeof(SchemaHistory).Name}");
                    }
                }
            }
            return factory;
        }

    }
}
