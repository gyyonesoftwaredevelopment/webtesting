﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace NHibernateWrapper
{
    public class MSSQLConnection : DBConnection
    {
        public MSSQLConnection(string host, string dbname, int port, string userid, string password, bool updateSchema = false) : base(host, dbname, port, userid, password,updateSchema)
        {
        }
        public override List<TableSchema> GetSchema()
        {
            var schema = new Dictionary<string, TableSchema>();

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                // Retrieve columns information
                var columnsQuery = @"
        SELECT 
            t.TABLE_NAME, 
            c.COLUMN_NAME, 
            c.DATA_TYPE, 
            c.IS_NULLABLE, 
            tc.CONSTRAINT_TYPE, 
            kcu2.TABLE_NAME AS RelatedTable, 
            kcu2.COLUMN_NAME AS RelatedColumn
        FROM 
            INFORMATION_SCHEMA.COLUMNS c
        LEFT JOIN 
            INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
        ON 
            c.TABLE_NAME = tc.TABLE_NAME
        AND 
            tc.CONSTRAINT_TYPE IN ('PRIMARY KEY', 'FOREIGN KEY')
        LEFT JOIN 
            INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
        ON 
            c.TABLE_NAME = kcu.TABLE_NAME
        AND 
            c.COLUMN_NAME = kcu.COLUMN_NAME
        AND 
            kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME
        LEFT JOIN 
            INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
        ON 
            rc.CONSTRAINT_NAME = kcu.CONSTRAINT_NAME
        LEFT JOIN 
            INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu2
        ON 
            kcu2.CONSTRAINT_NAME = rc.UNIQUE_CONSTRAINT_NAME
        WHERE 
            c.TABLE_CATALOG = @DatabaseName";

                using (var command = new SqlCommand(columnsQuery, connection))
                {
                    command.Parameters.AddWithValue("@DatabaseName", connection.Database);

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string tableName = reader["TABLE_NAME"].ToString();

                            if (!schema.ContainsKey(tableName))
                            {
                                schema[tableName] = new TableSchema
                                {
                                    TableName = tableName
                                };
                            }

                            var columnInfo = new ColumnSchema
                            {
                                ColumnName = reader["COLUMN_NAME"].ToString(),
                                DataType = reader["DATA_TYPE"].ToString(),
                                IsNullable = reader["IS_NULLABLE"].ToString() == "YES",
                                ConstraintType = reader["CONSTRAINT_TYPE"]?.ToString(),
                                RelatedTable = reader["RelatedTable"]?.ToString(),
                                RelatedColumn = reader["RelatedColumn"]?.ToString()
                            };

                            schema[tableName].Columns.Add(columnInfo);
                        }
                    }
                }
            }

            return schema.Values.ToList();
        }
        public override string DatabaseConnectionString
        {
            get
            {
                return $"Server={Host},{Port};User Id={UserId};Password={Password};Database={Database};";
            }

        }
        public override string ConnectionString
        {
            get
            {
                return $"Server={Host},{Port};User Id={UserId};Password={Password};Database=master;";
            }
       
        }
    }
}
