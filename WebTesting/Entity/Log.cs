﻿using FluentNHibernate.Mapping;
using NHibernateWrapper.Entity;


namespace Entity
{
    public class LogMap : SubclassMap<Log>
    {
        public LogMap()
        {
            Map(x => x.ResourceId).Length(50).Nullable();
            Map(x => x.LogType).Length(10).Nullable();
            Map(x => x.Message).CustomSqlType("text").Nullable();
            Map(x => x.MachineName).Length(255).Nullable();
            Map(x => x.CreatedDate).Nullable();
        }
    }
    public class Log : EntityIdentity
    {
        public virtual string ResourceId { set; get; }
        public virtual string LogType { set; get; }
        public virtual string Message { set; get; }
        public virtual string MachineName { set; get; }
        public virtual DateTime CreatedDate { set; get; }
    }
}
