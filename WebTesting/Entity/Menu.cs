﻿using FluentNHibernate.Mapping;
using NHibernateWrapper.Entity;
using OneOf;


namespace Entity
{
    public class MenuMap : SubclassMap<Menu>
    {
        public MenuMap()
        {
            // Mapping for simple properties
            References(x => x.Display).Nullable(); // Reference to the Localization entity
            Map(x => x.IconColor).Length(30).Nullable(); // IconColor with a max length of 50
            Map(x => x.TextColor).Length(30).Nullable(); // TextColor with a max length of 50
            Map(x => x.Icon).Length(255).Nullable(); // Icon with a max length of 255

            // Mapping for collections
            HasMany(x => x.SubMenu)
                .Cascade.All()
                .Inverse() // Assuming SubMenu is a child of Menu
                .KeyColumn("ParentId"); // You can adjust this to your actual foreign key


            HasManyToMany(x => x.AccessGroup)         
             .Cascade.All()         
             .LazyLoad();            

            // Many-to-many mapping for AccessUsers without creating FK
            HasManyToMany(x => x.AccessUsers)             
                .Cascade.All()           // No cascading              
                .LazyLoad();              // Lazy load for performance
        }
    }



    public class Menu : EntityAuditOrder
    { 
        public Menu()
        {
            SubMenu = new List<Menu>();
            AccessGroup = new HashSet<Groups>();
            AccessUsers = new HashSet<User>();
        }
        public virtual string ParentMenuId { set; get; }
        public virtual Localization Display { set; get; }
        public virtual string IconColor { set; get; }
        public virtual string TextColor { set; get; }
        public virtual string Icon { set; get; }
        public virtual string Url { set; get; }
        public virtual bool MVCFormat { set; get; }
        public virtual string ControllerName { set; get; }
        public virtual string ActionName { set; get; }
        public virtual string Parameters { set; get; }    
        public virtual IList<Menu> SubMenu { set; get; }    
        public virtual ISet<Groups> AccessGroup { set; get; }
        public virtual ISet<User> AccessUsers { set; get; }

    }
}
