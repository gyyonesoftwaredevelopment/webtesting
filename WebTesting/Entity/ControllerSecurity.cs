﻿using FluentNHibernate.Mapping;
using NHibernateWrapper.Entity;


namespace Entity
{
    public class ControllerSecurityMap : SubclassMap<ControllerSecurity>
    {
        public ControllerSecurityMap()
        {
            // Mapping for simple properties    
            Map(x => x.ActionName).Length(255).Nullable(); // TextColor with a max length of 50
            Map(x => x.ControllerName).Length(255).Nullable(); // Icon with a max length of 255

            HasManyToMany(x => x.AccessGroup)
             .Cascade.All()           // No cascading             
             .LazyLoad();              // Lazy load for performance

            // Many-to-many mapping for AccessUsers without creating FK
            HasManyToMany(x => x.AccessUsers)
                .Cascade.All()           // No cascading              
                .LazyLoad();              // Lazy load for performance
        }
    }
    public class ControllerSecurity:EntityAuditOrder
    {
        public ControllerSecurity() {
            AccessGroup = new HashSet<UserGroup>();
            AccessUsers = new HashSet<User>();
        }
        public override string Id { get => $"{ControllerName}/{ActionName}"; set => base.Id = $"{ControllerName}/{ActionName}"; }
        public virtual string ControllerName { set; get; }
        public virtual string ActionName { set; get; }
        public virtual ISet<UserGroup> AccessGroup { set; get; }
        public virtual ISet<User> AccessUsers { set; get; }
    }
}
