﻿using FluentNHibernate.Mapping;
using NHibernateWrapper.Entity;


namespace Entity
{
    public class LocalizationMap : SubclassMap<Localization>
    {
        public LocalizationMap()
        {
            Map(x => x.Key).Length(255).Nullable();  // Key field with a max length of 255
            Map(x => x.En).Length(1000).Nullable();  // English
            Map(x => x.Fr).Length(1000).Nullable();  // French
            Map(x => x.Es).Length(1000).Nullable();  // Spanish
            Map(x => x.De).Length(1000).Nullable();  // German
            Map(x => x.Cn).Length(1000).Nullable();  // Chinese (seems like a typo in your code with "De")

            // You can add additional language fields here with similar structure
        }
    }
    public class Localization : EntityIdentity
    {
        public virtual string Key { set; get; }
        // Properties for different languages
        public virtual string En { get; set; } // English
        public virtual string Fr { get; set; } // French
        public virtual string Es { get; set; } // Spanish
        public virtual string De { get; set; } // German
        public virtual string Cn { get; set; } // German
                                               // Add more languages as needed

        // This method retrieves the localized string based on the current language
        public virtual string GetLocalizedString(string languageCode)
        {
            return languageCode switch
            {
                "en" => En,
                "fr" => Fr,
                "es" => Es,
                "de" => De,
                "Cn" => De,
                // Add more cases for additional languages
                _ => En // Default to English if the language is not found
            };
        }

    }
}
