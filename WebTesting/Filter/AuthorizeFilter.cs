﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using NHibernateWrapper.Entity;
using Entity;
using Microsoft.AspNetCore.Mvc.Controllers;
using System.Reflection;
using Newtonsoft.Json;

namespace WebTesting.Filter
{
    public class SkipAuthorizeFilter : Attribute
    {

    }
    public class AuthorizeFilter : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string returnurl = context.HttpContext.Request.Query["returnUrl"];
            var filtermethod = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo.GetCustomAttributes<SkipAuthorizeFilter>();
            if(filtermethod.Any())
            {
                return;
            }
            // Custom authorization logic
            var user = context.HttpContext.User;

            // Check if the user is authenticated and belongs to a specific role
            if (user.Identity.IsAuthenticated)
            {
                Exception e = null;
                var actionDescriptor = context.ActionDescriptor;
                var controllerName = actionDescriptor.RouteValues["controller"];
                var actionName = actionDescriptor.RouteValues["action"];
                using(var session = Util.DB.OpenSession())
                {
                    try
                    {
                        string groups = user.Claims.FirstOrDefault(c => c.Type == "UserGroups")?.Value;
                        string userid = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
                        if (!string.IsNullOrEmpty(groups))
                        {
                            List<string>  accessGroup = JsonConvert.DeserializeObject<List<string>>(groups);
                            ControllerSecurity u = session.Get<ControllerSecurity>($"{controllerName}/{actionName}");
                            if(u != null && u.AccessGroup.Any(x=> accessGroup.Any(y=> y == x.Id)) || u.AccessUsers.Any(x=> x.Id == userid))
                            {
                                return;
                            }
                            else
                            {
                                context.Result = new RedirectToActionResult("Index", "Home", new { returnurl});
                            }
                        }                 
                     
                    }
                    catch(Exception ex)
                    {
                        Global.LogHandler.Error(ex);                     
                        context.Result = new RedirectToActionResult("Login", "Account", new { returnurl });
                    }
               

                }               
            }
            else
            {
                context.Result = new RedirectToActionResult("Login", "Account", new { returnurl });
            }
        }
    }

    public class DynamicAuthorizationHandler : AuthorizationHandler<DynamicAuthorizationRequirement>
    {
      

        public DynamicAuthorizationHandler()
        {
            
        }

        protected override async Task HandleRequirementAsync(
            AuthorizationHandlerContext context, DynamicAuthorizationRequirement requirement)
        {
          
        }
    }

    public class DynamicAuthorizationRequirement : IAuthorizationRequirement
    {
        public string RequiredPermission { get; }

        public DynamicAuthorizationRequirement(string requiredPermission)
        {
            RequiredPermission = requiredPermission;
        }
    }
}
