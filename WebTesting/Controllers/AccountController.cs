﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using WebTesting.Filter;
using Microsoft.AspNetCore.Http;
using System.Data;
using System.Net.Http;
using Newtonsoft.Json;
using NHibernateWrapper.Entity;
using NHibernate;

namespace WebTesting.Controllers
{
    public class AccountController : Controller
    {
        [SkipAuthorizeFilter]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [SkipAuthorizeFilter]
        public IActionResult Login(string returnUrl)
        {        
            return View("Login", returnUrl);
        }

        [HttpPost]
        [SkipAuthorizeFilter]
        public async Task<IActionResult> Login(string userid, string password, string returnUrl)
        {
            using(var session = Util.DB.OpenSession())
            {
                User user =  session.Get<User>(userid);
                if(user != null)
                {
                    NHibernateUtil.Initialize(user.UserGroups);
                    List<string> groups = user.UserGroups.Select(x=> x.Id).ToList();
                    if (user.Password == password)
                    {
                        // Create user identity
                        var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, userid),
                        new Claim("UserGroups", JsonConvert.SerializeObject(groups))
                     };
                        var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                        var principal = new ClaimsPrincipal(identity);
                        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                        if(!string.IsNullOrEmpty(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }

                }
              
            }
            return RedirectToAction("Login", new { returnUrl });
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}
