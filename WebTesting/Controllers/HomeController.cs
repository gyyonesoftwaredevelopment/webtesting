using FluentNHibernate.Conventions;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Reflection;
using WebTesting.Filter;
using WebTesting.Models;

namespace WebTesting.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
    
        public IActionResult Index()
        {
            //Global.LogHandler.Debug("Test");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult JsonForm()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        private IEnumerable<ControllerActionInfo> GetControllersAndActions()
        {
            // Get the assembly where your controllers are defined (typically the current assembly)
            var assembly = Assembly.GetExecutingAssembly();

            // Find all types that are subclasses of 'Controller'
            var controllerTypes = assembly.GetTypes()
                .Where(type => typeof(Controller).IsAssignableFrom(type) && !type.IsAbstract)
                .ToList();

            // For each controller type, find its public methods that are actions
            var controllerActions = controllerTypes.Select(controllerType => new ControllerActionInfo
            {
                ControllerName = controllerType.Name.Replace("Controller", ""),
                ActionNames = controllerType.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public)
                                            .Where(method => typeof(ActionResult).IsAssignableFrom(method.ReturnType) && !method.GetCustomAttributes<SkipAuthorizeFilter>().Any())
                                            .Select(method => method.Name)
                                            .ToList()
            });

            return controllerActions;
        }

        public class ControllerActionInfo
        {
            public string ControllerName { get; set; }
            public List<string> ActionNames { get; set; }
        }
    }
}
