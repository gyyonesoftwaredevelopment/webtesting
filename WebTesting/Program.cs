using System.Data.Common;
using System.Reflection;
using System.Xml;
using Entity;
using Microsoft.AspNetCore.Authentication.Cookies;
using NHibernateWrapper;
using NHibernateWrapper.Entity;
using WebTesting;
using WebTesting.DBScript;
using WebTesting.Filter;
Global.InitLogHandler();
Global.LogHandler.Debug("Starting Web");
var builder = WebApplication.CreateBuilder(args);
MSSQLConnection dBConnection = new MSSQLConnection("192.168.56.5", "WEBDB", 1433, "sa", "P@ssw0rd");
//MySQLConnection dBConnection = new MySQLConnection("192.168.103.115", "TestWeb", 52005, "root", "M@ster5am");
dBConnection.UpdateSchema = true;
Version version = Assembly.GetExecutingAssembly().GetName().Version;
var essionfactory = DBFactory.CreateMSSQLSessionFactory(dBConnection,$"{version.ToString()}", Assembly.GetAssembly(typeof(EntityIdentity)), Assembly.GetAssembly(typeof(Log)));
Util.DB = new DB(essionfactory);
DBUpHandling.ExecuteScript(DBUpHandling.DBType.MySQL);
// Add services to the container.

builder.Services.AddControllersWithViews(options =>
{
    // Register CustomAuthorize globally
    options.Filters.Add<AuthorizeFilter>();
});
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options =>
    {
        // Set login path for redirecting unauthorized users
        options.LoginPath = "/Account/Login";

        // Set cookie properties for security
        options.Cookie.HttpOnly = true;           // Prevents JavaScript from accessing the cookie
        options.Cookie.SecurePolicy = CookieSecurePolicy.Always; // Sends cookie only over HTTPS
        options.Cookie.SameSite = SameSiteMode.Strict; // Prevents CSRF attacks
        options.Cookie.Name = "SecureAuthApp";    // Custom cookie name
        options.Cookie.MaxAge = TimeSpan.FromMinutes(30); // Cookie expiration time

        // Expire session after a period of inactivity
        options.SlidingExpiration = true;

        // Logout and redirect to the login page when session expires
        options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
    });
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
