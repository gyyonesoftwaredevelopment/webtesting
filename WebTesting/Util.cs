﻿using NHibernateWrapper;

namespace WebTesting
{
    public static class Util
    {
        public static DB DB { get; set; }
        public static Config Config { set; get; }

    }
    public class Config
    {
        public Config() { }
        public DBConnection Connection { get; set; }
    }
}
