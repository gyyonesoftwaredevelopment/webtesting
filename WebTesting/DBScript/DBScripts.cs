﻿using Entity;
using NHibernate;
using NHibernate.Hql.Ast.ANTLR.Tree;
using NHibernateWrapper.Entity;
using System.Text.RegularExpressions;
using static WebTesting.DBScript.DBUpHandling;

namespace WebTesting.DBScript
{
    public static class DefaultUsers
    {
        public static User systemUser = new User() { UserId = "system", UserName = "System", Password = "P@ssw0rd", CreatedBy = null, UpdatedBy = null };
        public static User adminUser = new User() { UserId = "admin", UserName = "Admin", Password = "P@ssw0rd", CreatedBy = null, UpdatedBy = null };
        public static User demoUser = new User() { UserId = "demo", UserName = "Demo", Password = "123456", CreatedBy = null, UpdatedBy = null };
    }
    public static class DefaultUserGroups
    {
        public static UserGroup Admin = new UserGroup() { GroupId = "administrator" , GroupDesc = "Administrotor" , Users = new HashSet<User>() { DefaultUsers.adminUser } };
        public static UserGroup Users = new UserGroup() { GroupId = "users", GroupDesc = "Users", Users = new HashSet<User>() { DefaultUsers.demoUser } };
    }
    public static class DBScripts
    {
      
        public static List<Action<NHibernate.ISession>> InitScript(DBType dbType = DBType.MSSQL)
        {
            List<Action<NHibernate.ISession>> Scripts = new List<Action<NHibernate.ISession>>();

  
    
            #region 1.0.0.0            
            Scripts.Add((session) =>
            {
                session.SaveOrUpdate(DefaultUsers.systemUser);
                session.SaveOrUpdate(DefaultUsers.adminUser);
                session.SaveOrUpdate(DefaultUserGroups.Admin);

                session.SaveOrUpdate(new ScriptHistory() { Version = "1.0.0.0", SeqNo = Scripts.Count});
            });

            #endregion

            #region Example        
            /* In Debug Mode,  last script will keep execute even that it already execute
            Scripts.Add((session) =>
            {          
                session.SaveOrUpdate(new ScriptHistory() { Version = "1.0.0.0", SeqNo = Scripts.Count, CreatedBy = system, UpdatedBy = system });
            });
            */
            #endregion

            return Scripts;
        }
    }

    
}
