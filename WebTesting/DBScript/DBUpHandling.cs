﻿
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using static WebTesting.Controllers.HomeController;
using System.Reflection;
using Entity;
using NHibernateWrapper.Entity;
using WebTesting.Filter;
namespace WebTesting.DBScript
{
    public static class DBUpHandling
    {
        public enum DBType
        {
            MSSQL,
            MySQL,
        }


        public static void ExecuteScript(DBType dbType = DBType.MSSQL)
        {
            Exception exception = null;
            List<Action<NHibernate.ISession>> scripts = DBScripts.InitScript(dbType);
            int TotalSchema = 0;
            if(Util.DB.Session((session)=> {
                TotalSchema = session.QueryOver<ScriptHistory>().RowCount();
            }, ref exception))
            {
                for(int x = TotalSchema > 0 ? TotalSchema - 1 : 0; x < scripts.Count; x++)
                {
                    Util.DB.SessionSafe(scripts[x], ref exception);
                    if (exception != null)
                    {
                        Global.LogHandler.Error(exception);
                        throw exception;
                    }
                }
            }
            else
            {
                Global.LogHandler.Error(exception);
            }
            
            Exception e = null;
            Util.DB.SessionSafe((session) => {
                var assembly = Assembly.GetExecutingAssembly();
                // Find all types that are subclasses of 'Controller'
                var controllerTypes = assembly.GetTypes()
                    .Where(type => typeof(Controller).IsAssignableFrom(type) && !type.IsAbstract)
                    .ToList();

                // For each controller type, find its public methods that are actions
                var controllerActions = controllerTypes.Select(controllerType => new ControllerActionInfo
                {
                    ControllerName = controllerType.Name.Replace("Controller", ""),
                    ActionNames = controllerType.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public)
                                                .Where(method =>  !method.GetCustomAttributes<SkipAuthorizeFilter>().Any())
                                                .Select(method => method.Name)
                                                .ToList()
                });
#if DEBUG
                if(TotalSchema <= scripts.Count)
#else
                if(TotalSchema != scripts.Count)
#endif
                {
                    foreach (var c in controllerActions)
                    {
                        foreach (var actionname in c.ActionNames)
                        {
                            ControllerSecurity controllerSecurity = session.Get<ControllerSecurity>($"{c.ControllerName}/{actionname}");
                            if (controllerSecurity == null)
                            {
                                controllerSecurity = new ControllerSecurity()
                                {
                                    ActionName = actionname,
                                    ControllerName = c.ControllerName,

                                };

                            }
                            if (!controllerSecurity.AccessGroup.Contains(DefaultUserGroups.Admin))
                            {
                                controllerSecurity.AccessGroup.Add(session.Load<UserGroup>(DefaultUserGroups.Admin.Id));
                            }
                            session.SaveOrUpdate(controllerSecurity);


                        }
                    }
                }
                

              
            }, ref e);
        }

    }
    public class VersionConverter
    {
        private const int MajorMax = 1000;
        private const int MinorMax = 1000;
        private const int PatchMax = 1000;
        private const int BuildMax = 10000;

        // Converts a version string to an integer
        public static long VersionToNumber(string version)
        {
            if (string.IsNullOrWhiteSpace(version))
                throw new ArgumentException("Version cannot be null or empty.");

            var parts = version.Split('.');
            if (parts.Length != 4)
                throw new ArgumentException("Version must be in Major.Minor.Patch.Build format.");

            if (!int.TryParse(parts[0], out int major) || major >= MajorMax ||
                !int.TryParse(parts[1], out int minor) || minor >= MinorMax ||
                !int.TryParse(parts[2], out int patch) || patch >= PatchMax ||
                !int.TryParse(parts[3], out int build) || build >= BuildMax)
            {
                throw new ArgumentException("Version parts exceed allowed limits.");
            }

            return major * 1000000000 + minor * 1000000 + patch * 1000 + build;
        }

        // Converts an integer to a version string
        public static string NumberToVersion(long versionInt)
        {
            if (versionInt < 0)
                throw new ArgumentException("Version integer cannot be negative.");

            long build = versionInt % 1000;
            long patch = (versionInt / 1000) % 100000;
            long minor = (versionInt / 1000000) % 1000;
            long major = (versionInt / 1000000000) % 1000;

            return $"{major}.{minor}.{patch}.{build}";
        }
    }
}
