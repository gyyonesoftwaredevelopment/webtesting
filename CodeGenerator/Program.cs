﻿// See https://aka.ms/new-console-template for more information
using Scriban;

Console.WriteLine("Hello, World!");
List<string> ProductList = new List<string>();
var template = Template.Parse(@"
<ul id='products'>
  {{ for product in products }}
    <li>
      <h2>{{ product.name }}</h2>
           Price: {{ product.price }}
           {{ product.description | string.truncate 15 }}
    </li>
  {{ end }}
</ul>
");
var result = template.Render(new { Products = ProductList });