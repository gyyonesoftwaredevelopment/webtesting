﻿using System;
using System.IO;
using System.Reflection;
using System.Xml;


public static class Global
{
    public static log4net.ILog LogHandler { set; get; }

    public static void InitLogHandler()
    {
        XmlDocument log4netConfig = new XmlDocument();
        log4netConfig.Load(File.OpenRead(AppDomain.CurrentDomain.BaseDirectory + "log4net.config"));
        var repo = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(),
                   typeof(log4net.Repository.Hierarchy.Hierarchy));
        log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
        LogHandler = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }


}