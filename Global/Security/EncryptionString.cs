﻿using System;
using System.IO;
using System.Security.Cryptography;

public static class EncryptionString
{
    private static int KeySize = 256;
    private static int BlockSize = 128;
    public static string Decrypt(string text, string password)
    {
        if (string.IsNullOrWhiteSpace(password) || string.IsNullOrWhiteSpace(text))
        {
            return "";
        }
        // Create an AesManaged object
        // with the specified key and IV.
        using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
        {
            aesAlg.KeySize = KeySize;
            aesAlg.BlockSize = BlockSize;
            aesAlg.Key = GenerateKey(password);
            aesAlg.IV = GenerateIV(password);

            // Create an encryptor to perform the stream transform.
            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for encryption.
            using (MemoryStream msEncrypt = new MemoryStream(System.Convert.FromBase64String(text)))
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader swEncrypt = new StreamReader(csEncrypt))
                    {
                        //Write all data to the stream.
                        return swEncrypt.ReadToEnd();
                    }
                }
            }
        }
    }

    public static string Enrypt(string text, string password)
    {
        if (string.IsNullOrWhiteSpace(password) || string.IsNullOrWhiteSpace(text))
        {
            return "";
        }
        // Create an AesManaged object
        // with the specified key and IV.
        using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
        {
            aesAlg.KeySize = KeySize;
            aesAlg.BlockSize = BlockSize;
            aesAlg.Key = GenerateKey(password);
            aesAlg.IV = GenerateIV(password);

            // Create an encryptor to perform the stream transform.
            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

            // Create the streams used for encryption.
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        //Write all data to the stream.
                        swEncrypt.Write(text);
                    }
                    byte[] encrypted = msEncrypt.ToArray();
                    return Convert.ToBase64String(encrypted);
                }
            }
        }

    }

    public static string Enrypt(string v, object cryptoKey)
    {
        throw new NotImplementedException();
    }

    private static byte[] GenerateKey(string password)
    {
        return new byte[] { 18, 17, 63, 11, 24, 26, 85, 45, 114, 184, 27, 162, 37, 112, 222, 209, 241, 24, 175, 144, 173, 137, 196, 29, 24, 26, 17, 217, 131, 131, 42, 79 };
        //byte[] ivgen = Encoding.Unicode.GetBytes(password);
        //byte[] iv = new byte[KeySize / 8];
        //for (int x = 0; x < KeySize / 8; x++)
        //{
        //    if (ivgen.Length > x)
        //    {
        //        iv[x] = ivgen[x];
        //    }
        //    else
        //    {
        //        iv[x] = (byte)(x > 255 ? x : 1);
        //    }
        //}

        //return iv;
    }

    private static byte[] GenerateIV(string password)
    {
        return new byte[] { 146, 64, 191, 111, 23, 3, 113, 119, 231, 121, 211, 112, 12, 32, 235, 76 };
        //byte[] ivgen = Encoding.Unicode.GetBytes(password);
        //byte[] iv = new byte[BlockSize / 8];
        //for (int x = 0; x < BlockSize / 8; x++)
        //{
        //    if (ivgen.Length > x)
        //    {
        //        iv[x] = ivgen[x];
        //    }
        //    else
        //    {
        //        iv[x] = (byte)(x > 255 ? x : 1);
        //    }
        //}

        //return iv;
    }
}