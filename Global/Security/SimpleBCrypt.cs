﻿using BCrypt.Net;
using System.Security.Cryptography;
using System.Text;

public static class SimpleBCrypt
{
    public static string EncryptOneWay(string text)
    {
        var hashedPassword = BCrypt.Net.BCrypt.HashPassword(text, BCrypt.Net.BCrypt.GenerateSalt());

        return hashedPassword;
    }

    public static bool Verify(string text, string hash)
    {
        return BCrypt.Net.BCrypt.Verify(text, hash);

    }
}