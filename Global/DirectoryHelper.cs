﻿using System.IO;
using System.Linq;

public static class DirectoryHelper
{
    public static string[] GetFiles(string FilePath, string[] Extension, SearchOption searchOption)
    {
        //string Files2 = Directory.GetFiles();
        var files = Directory.GetFiles(FilePath, "*.*", searchOption)
            .Where(f => Extension.Contains(Path.GetExtension(f).ToLower())).ToArray();
        return files;
    }
    public static void Delete(string source)
    {
        if (IsFile(source))
        {
            File.Delete(source);
        }
        else
        {
            var files = Directory.GetFiles(source);
            foreach (var file in files)
            {
                //Copy File ** Not Include Directory
                File.Delete(file);
            }
            var Directories = Directory.GetDirectories(source);
            foreach (var DirectoryFile in Directories)
            {
                Delete(DirectoryFile);
            }
        }

    }
    public static void Move(string source, string destination)
    {
        CreateDirectory(destination);
        if (IsFile(source))
        {
            if (File.Exists(destination))
            {
                File.Delete(destination);
            }
            File.Move(source, destination);
        }
        else
        {
            var files = Directory.GetFiles(source);

            foreach (var file in files)
            {
                var filename = Path.GetFileName(file);
                var DestinationFile = destination + @"\" + filename;
                if (File.Exists(DestinationFile))
                {
                    File.Delete(DestinationFile);
                }
                //Copy File ** Not Include Directory
                File.Move(file, DestinationFile);
            }
            var Directories = Directory.GetDirectories(source);
            foreach (var DirectoryFile in Directories)
            {
                Move(DirectoryFile, destination + @"\" + Path.GetFileName(DirectoryFile));
            }
        }

    }
    public static void Copy(string Source, string Destination)
    {
        var files = Directory.GetFiles(Source);
        CreateDirectory(Destination + @"\");
        foreach (var file in files)
        {
            var filename = Path.GetFileName(file);
            var DestinationFile = Destination + @"\" + filename;
            if (File.Exists(DestinationFile))
            {
                File.Delete(DestinationFile);
            }
            //Copy File ** Not Include Directory
            File.Copy(file, DestinationFile);
        }
        var Directories = Directory.GetDirectories(Source);
        foreach (var DirectoryFile in Directories)
        {
            Copy(DirectoryFile, Destination + @"\" + Path.GetFileName(DirectoryFile));
        }
    }

    public static bool IsFile(string path)
    {
        // get the file attributes for file or directory
        FileAttributes attr = File.GetAttributes(path);

        //detect whether its a directory or file
        if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
            return false;
        else
            return true;
    }
    public static void CreateDirectory(string path)
    {
        if (!Directory.Exists(Path.GetDirectoryName(path)))
        {
            path = Path.GetDirectoryName(path);
            path = path.Replace("\\", "/");
            var NetworkPath = path.IndexOf("/");
            var filedirectory = path.Split('/');
            var directory = "";
            for (var x = 0; x < filedirectory.Length; x++)
            {
                directory += filedirectory[x] + "/";

                if (!Directory.Exists(directory) && filedirectory[x] != "")
                {
                    if (!(NetworkPath == 0 && (x <= 2)))
                    {
                        Directory.CreateDirectory(directory);
                    }
                }
            }
        }
    }
}